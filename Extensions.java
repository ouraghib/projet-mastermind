public class Extensions {
    //.........................................................................
    // EXTENSIONS
    //.........................................................................
    /** pré-requis : cod est une matrice, rep est une matrice à 2 colonnes,
     0 <= nbCoups < cod.length, nbCoups < rep.length et les éléments de cod sont
     des entiers de 0 à tabCouleurs.length -1

     action : affiche les nbCoups premières lignes de cod (sous forme de mots en
     utilisant le tableau tabCouleurs) et de rep
     */
    public static void affichePlateau(int [][] cod, int [][] rep, int nbCoups, char[] tabCouleurs){
        Ut.afficher("Codes :");
        Ut.sautLigne();
        String code = new String();
        int[] tabTempC = new int[cod[0].length];
        for (int i=0;i<nbCoups;i++){
                for(int o=0;o<cod[1].length;o++){
                    tabTempC[o]=cod[i][o];
                    code = MasterMindBase.entiersVersMot(tabTempC,tabCouleurs);
                    Ut.afficherSL(code);
                }
                Ut.sautLigne();
            }

        Ut.afficher("Reponses :");
        Ut.sautLigne();
        int[] tabTempR = new int[rep[0].length];
        String reponse = new String();
        for (int i=0;i<nbCoups;i++){
            for(int o=0;o<rep[1].length;o++){
                tabTempR[o]=rep[i][o];
                reponse = MasterMindBase.entiersVersMot(tabTempR,tabCouleurs);
                Ut.afficherSL(reponse);
            }
            Ut.sautLigne();
        }
        }


        public static void main(String[] args) {
        int[][] cod= {{0,0,0,1},{1,2,4,3}};
            int[][] rep= {{0,1},{2,1}};
            int nbCoups=2;
            char[] tabCouleurs = {'R','V','B','O'};
affichePlateau(cod,rep,nbCoups,tabCouleurs);
        }
    }

