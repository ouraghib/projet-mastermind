import java.util.*;
import java.lang.*;

public class MasterMindBase {

    //.........................................................................
    // OUTILS DE BASE
    //.........................................................................

    // fonctions classiques sur les tableaux

    /** pré-requis : nb >= 0
     résultat : un tableau de nb entiers égaux à val
     */
    public static int[] initTab(int nb, int val){
        int[] tab= new int[nb];
        for (int i=0;i<nb; i++){
            tab[i]=val;
        }
        return tab;
    }

    //______________________________________________

    /** pré-requis : aucun
     résultat : une copie de tab
     */
    public static int[] copieTab(int[] tab){
        int[] copieTab= new int[tab.length];
        for (int i=0;i<tab.length;i++){
            copieTab[i]=tab[i];
        }
        return copieTab;

    }

    //______________________________________________

    /** pré-requis : aucun
     résultat : la liste des éléments de t entre parenthèses et séparés par des virgules
     */
    public static String listElem(char[] t){
        String liste="(";
        for(int i=0;i<t.length;i++){
            liste = liste +t[i];
            if (i!=t.length-1){
                liste = liste + ", ";
            }
            else{
                liste = liste + ")";
            }
        }
        return liste;
    }

    //______________________________________________

    /** pré-requis : aucun
     résultat : le plus grand indice d'une case de t contenant c s'il existe, -1 sinon
     */
    public static int plusGrandIndice(char[] t, char c){
        int IndiceMax=-1;
        for (int i=0; i<t.length; i++){
            if (c==t[i]){
                IndiceMax=i;
            }
        }
        return IndiceMax;
    }
    //______________________________________________

    /** pré-requis : aucun
     résultat : vrai ssi c est un élément de t
     stratégie : utilise la fonction plusGrandIndice
     */
    public static boolean estPresent(char[] t, char c){
        if(plusGrandIndice(t,c)==-1) {
            return false;
        }
        else {
            return true;
        }
    }

    //______________________________________________

    /** pré-requis : aucun
     action : affiche un doublon et 2 de ses indices dans t s'il en existe
     résultat : vrai ssi les éléments de t sont différents
     stratégie : utilise la fonction plusGrandIndice
     */
    public static boolean elemDiff(char[] t){
        for(int i=0; i<t.length;i++) {
            if (plusGrandIndice(t,t[i])!=-1){
                for (int j=0;j<t.length;j++){
                    if (t[j]==t[i] && j!=i){
                        System.out.println("doublon : "+t[j]+"\nindices des doublons : "+j+","+i);
                        return false;
                    }
                }
            }
        }
        return true;
    }

    //______________________________________________

    /** pré-requis : t1.length = t2.length
     résultat : vrai ssi t1 et t2 contiennent la même suite d'entiers
     */
    public static boolean sontEgaux(int[] t1, int[] t2){
        for(int i=0;i<t1.length;i++){
            if (t1[i]!=t2[i]){
                return false;
            }
        }
        return true;
    }

    //______________________________________________

    // Dans toutes les fonctions suivantes, on a comme pré-requis implicites sur les paramètres lgCode, nbCouleurs et tabCouleurs :
    // lgCode > 0, nbCouleurs > 0, tabCouleurs.length > 0 et les éléments de tabCouleurs sont différents

    // fonctions sur les codes pour la manche Humain

    /** pré-requis : aucun
     résultat : un tableau de lgCode entiers choisis aléatoirement entre 0 et nbCouleurs-1
     */
    public static int[] codeAleat(int lgCode, int nbCouleurs){
        int[] tab=new int[lgCode];
        for (int i=0;i<lgCode;i++){
            int nbAleatoire= Ut.randomMinMax(0, nbCouleurs-1);
            tab[i] = nbAleatoire;
        }
        return tab;
    }


    //____________________________________________________________

    /** pré-requis : aucun
     action : si codMot n'est pas correct, affiche pourquoi
     résultat : vrai ssi codMot est correct, c'est-à-dire de longueur lgCode et ne contenant que des éléments de tabCouleurs
     */
    public static boolean codeCorrect(String codMot, int lgCode, char[] tabCouleurs){
        boolean booleen=false;
        for (int i=0;i<codMot.length();i++){
            booleen = false;
            for(int j=0;j<tabCouleurs.length;j++){
                if (tabCouleurs[j]==codMot.charAt(i)){
                    booleen = true;
                }
            }
            if(booleen == false && codMot.length()!=lgCode){
                System.out.println("codMot ne contient pas que des éléments de tabCouleurs et codMot n'est pas de longueur lgCode");
                return booleen;
            }
            else if(booleen==false){
                System.out.println("codMot ne contient pas que des éléments de tabCouleurs");
                return booleen;
            }
        }
        if (codMot.length()!=lgCode){
            System.out.println("codMot n'est pas de longueur lgCode");
            booleen=false;
        }
        return booleen;
    }

    //____________________________________________________________

    /** pré-requis : les caractères de codMot sont des éléments de tabCouleurs
     résultat : le code codMot sous forme de tableau d'entiers en remplaçant chaque couleur par son indice dans tabCouleurs
     */
    public static int[] motVersEntiers(String codMot, char[] tabCouleurs){
        int[] tab = new int[codMot.length()];
        for (int i=0;i<codMot.length();i++){
            for(int j=0;j<tabCouleurs.length;j++){
                if (tabCouleurs[j]==codMot.charAt(i)){
                    tab[i]=j;
                }
            }
        }
        return tab;
    }

    //____________________________________________________________

    /** pré-requis : aucun
     action : demande au joueur humain de saisir la (nbCoups + 1)ème proposition de code sous forme de mot, avec re-saisie éventuelle jusqu'à ce
     qu'elle soit correcte (le paramètre nbCoups ne sert que pour l'affichage)
     résultat : le code saisi sous forme de tableau d'entiers
     */
    public static int[] propositionCodeHumain(int nbCoups, int lgCode, char[] tabCouleurs){
        int[] code=new int[lgCode];
        String codeMot ="";
        nbCoups++;
        do {
            System.out.println("Saisir l'essais numéro :"+nbCoups);
            codeMot = Ut.saisirChaine();
        }while (!codeCorrect(codeMot,lgCode,tabCouleurs));
        System.out.println(codeMot);
        code = motVersEntiers(codeMot,tabCouleurs);
        return code;
    }

    //____________________________________________________________

    /** pré-requis : cod1.length = cod2.length
     résultat : le nombre d'éléments communs de cod1 et cod2 se trouvant au même indice
     Par exemple, si cod1 = (1,0,2,0) et cod2 = (0,1,0,0) la fonction retourne 1 (le "0" à l'indice 3)
     */
    public static int nbBienPlaces(int[] cod1,int[] cod2){
        int nbElements=0;
        for (int i=0;i<cod1.length;i++) {
            if (cod1[i]==cod2[i]) {
                nbElements=nbElements+1;
            }
        }
        return nbElements;
    }

    //____________________________________________________________

    /** pré-requis : les éléments de cod sont des entiers de 0 à nbCouleurs-1
     résultat : un tableau de longueur nbCouleurs contenant à chaque indice i le nombre d'occurrences de i dans cod
     Par exemple, si cod = (1,0,2,0) et nbCouleurs = 6 la fonction retourne (2,1,1,0,0,0)
     */
    public static int[] tabFrequence(int[] cod, int nbCouleurs){
        int[] frequence=new int[nbCouleurs];
        for (int i=0;i<cod.length;i++){
            frequence[cod[i]]=frequence[cod[i]] + 1;
        }
        return frequence;
    }

    //____________________________________________________________

    /** pré-requis : les éléments de cod1 et cod2 sont des entiers de 0 à nbCouleurs-1
     résultat : le nombre d'éléments communs de cod1 et cod2, indépendamment de leur position
     Par exemple, si cod1 = (1,0,2,0) et cod2 = (0,1,0,0) la fonction retourne 3 (2 "0" et 1 "1")
     */
    public static int nbCommuns(int[] cod1,int[] cod2, int nbCouleurs){
        int commun=0;
        if (cod1.length!=0 && cod2.length!=0) {
            int[] tab1=tabFrequence(cod1,nbCouleurs);
            int[] tab2=tabFrequence(cod2,nbCouleurs);
            for (int i=0;i<nbCouleurs;i++){
                if(tab1[i]> tab2[i]) {
                    commun =commun + tab2[i];
                }
                else{
                    commun=commun+tab1[i];
                }
            }
        }
        return commun;
    }

    //____________________________________________________________

    /** pré-requis : cod1.length = cod2.length et les éléments de cod1 et cod2 sont des entiers de 0 à nbCouleurs-1
     résultat : un tableau de 2 entiers contenant à l'indice 0 (resp. 1) le nombre d'éléments communs de cod1 et cod2
     se trouvant (resp. ne se trouvant pas) au même indice
     Par exemple, si cod1 = (1,0,2,0) et cod2 = (0,1,0,0) la fonction retourne (1,2) : 1 bien placé (le "0" à l'indice 3)
     et 2 mal placés (1 "0" et 1 "1")
     */
    public static int[] nbBienMalPlaces(int[] cod1,int[] cod2, int nbCouleurs) {
        int[] tabIndice = new int[2];
        int bienPlace= nbBienPlaces(cod1,cod2);
        int nbMalPlace=nbCommuns(cod1,cod2,nbCouleurs)-bienPlace;
        tabIndice[0]=bienPlace;
        tabIndice[1]=nbMalPlace;
        return tabIndice;
    }




    //____________________________________________________________

    //.........................................................................
    // MANCHEHUMAIN
    //.........................................................................

    /** pré-requis : numMache >= 1
     action : effectue la (numManche)ème manche où l'ordinateur est le codeur et l'humain le décodeur
     (le paramètre numManche ne sert que pour l'affichage)
     résultat :
     - un nombre supérieur à nbEssaisMax, calculé à partir du dernier essai du joueur humain (cf. sujet),
     s'il n'a toujours pas trouvé au bout du nombre maximum d'essais
     - sinon le nombre de codes proposés par le joueur humain
     */
    public static int mancheHumain(int lgCode, char[] tabCouleurs, int numManche, int nbEssaisMax){
        int nbCouleurs= tabCouleurs.length;
        int[] codeAleat=codeAleat(lgCode,nbCouleurs);
        int score=0;
        int nbCoups=0;
        int[] derniereProposition;
        int[] nbBienMalPlaces=new int[lgCode];
        System.out.println("\n"+"manche numéro: "+numManche);
        while (score<nbEssaisMax){
            score++;
            derniereProposition=propositionCodeHumain(nbCoups,lgCode,tabCouleurs);
            nbCoups++;
            nbBienMalPlaces= nbBienMalPlaces(derniereProposition,codeAleat,nbCouleurs);
            System.out.println(Arrays.toString(nbBienMalPlaces));
            if (sontEgaux(derniereProposition,codeAleat)){
                System.out.print("votre score sur cette manche est de: "+ score);
                return score;
            }
        }
        score = nbEssaisMax + nbBienMalPlaces[1]+2*(lgCode-(nbBienMalPlaces[0]+nbBienMalPlaces[1]));
        System.out.print("votre score sur cette manche est de " + score);
        return score;
    }

    //____________________________________________________________

    //...................................................................
    // FONCTIONS COMPLÉMENTAIRES SUR LES CODES POUR LA MANCHE ORDINATEUR
    //...................................................................

    /** pré-requis : les éléments de cod sont des entiers de 0 à tabCouleurs.length-1
     résultat : le code cod sous forme de mot d'après le tableau tabCouleurs
     */
    public static String entiersVersMot(int[] cod, char[] tabCouleurs){
        char[] entierVersTabChar=new char[cod.length];
        for (int i=0;i<cod.length;i++){
            entierVersTabChar[i]=tabCouleurs[cod[i]];
        }
        String mot = new String(entierVersTabChar);

        return mot;

    }

    //___________________________________________________________________

    /** pré-requis : rep.length = 2
     action : si rep n'est pas  correcte, affiche pourquoi, sachant que rep[0] et rep[1] sont
     les nombres de bien et mal placés resp.
     résultat : vrai ssi rep est correct, c'est-à-dire rep[0] et rep[1] sont >= 0 et leur somme est <= lgCode
     */
    public static boolean repCorrecte(int[] rep, int lgCode){
        if (rep[1]>=0 && rep[0]>=0 && rep[0]+rep[1]<=lgCode){
            return true;
        } else if (rep[1]<0 && rep[0]<0){
            Ut.afficherSL("ERREUR : Les nombre de mal places et bien places respectivement doivent etre strictement positif ");
        } else if (rep[0]<0){
            Ut.afficherSL("ERREUR : Le nombre de bien places doit etre strictement positif ");
        } else if (rep[1]<0) {
            Ut.afficherSL("ERREUR : Le nombre de mal places doit etre strictement positif ");
        } else if (rep[0]+rep[1]>lgCode) {
            Ut.afficherSL("ERREUR : La somme de vos 2 nombres : bien places et mal places doit etre inférieur ou egal a "+lgCode);
        }
        return false;
    }

    //___________________________________________________________________

    /** pré-requis : aucun
     action : demande au joueur humain de saisir les nombres de bien et mal placés,
     avec re-saisie éventuelle jusqu'à ce qu'elle soit correcte
     résultat : les réponses du joueur humain dans un tableau à 2 entiers
     */
    public static int[] reponseHumain(int lgCode){
        int[]rep= new int[2];
        Ut.afficher("Saisissez le nombre de bien places: ");
        rep[0]=Ut.saisirEntier();
        Ut.afficher("Saisissez le nombre de mal places: ");
        rep[1]=Ut.saisirEntier();
        while(!repCorrecte(rep,lgCode)){
            Ut.afficher("Ressaisissez le nombre de bien places: ");
            rep[0]=Ut.saisirEntier();
            Ut.afficher("Ressaisissez le nombre de mal places: ");
            rep[1]=Ut.saisirEntier();
        }
        return rep;
    }

    //___________________________________________________________________

    /**CHANGE : action si le code suivant n'existe pas
     *************************************************
     pré-requis : les éléments de cod1 sont des entiers de 0 à nbCouleurs-1
     action/résultat : met dans cod1 le code qui le suit selon l'ordre lexicographique (dans l'ensemble
     des codes à valeurs de 0 à nbCouleurs-1) et retourne vrai si ce code existe,
     sinon met dans cod1 le code ne contenant que des "0" et retourne faux
     */
    public static boolean passeCodeSuivantLexico(int[] cod1, int nbCouleurs) {
        int indiceCod1 = cod1.length - 1;
        while (indiceCod1 >= 0) {
            if (cod1[indiceCod1] < nbCouleurs - 1) {
                cod1[indiceCod1] = cod1[indiceCod1] + 1;
                return true;
            } else {
                cod1[indiceCod1] = 0;
            }
            indiceCod1--;
        }
        return false;
    }

    //___________________________________________________________________

    /**CHANGE : ajout du paramètre cod1 et modification des spécifications
     *********************************************************************
     pré-requis : cod est une matrice à cod1.length colonnes, rep est une matrice à 2 colonnes, 0 <= nbCoups < cod.length,
     nbCoups < rep.length et les éléments de cod1 et de cod sont des entiers de 0 à nbCouleurs-1
     résultat : vrai ssi cod1 est compatible avec les nbCoups premières lignes de cod et de rep,
     c'est-à-dire que si cod1 était le code secret, les réponses aux nbCoups premières
     propositions de cod seraient les nbCoups premières réponses de rep resp.
     */
    public static boolean estCompat(int[] cod1, int[][] cod, int[][] rep, int nbCoups, int nbCouleurs) {
        for (int i = 0; i < nbCoups; i++) {
            int[] nbBienMalPlaces = nbBienMalPlaces(cod1, cod[i], nbCouleurs);
            if (!sontEgaux(nbBienMalPlaces, rep[i])) {
                return false;
            }
        }
        return true;
    }

    //___________________________________________________________________

    /**CHANGE : renommage de passePropSuivante en passeCodeSuivantLexicoCompat,
     ajout du paramètre cod1 et modification des spécifications
     **************************************************************************
     pré-requis : cod est une matrice à cod1.length colonnes, rep est une matrice à 2 colonnes, 0 <= nbCoups < cod.length,
     nbCoups < rep.length et les éléments de cod1 et de cod sont des entiers de 0 à nbCouleurs-1
     action/résultat : met dans cod1 le plus petit code (selon l'ordre lexicographique (dans l'ensemble
     des codes à valeurs  de 0 à nbCouleurs-1) qui est à la fois plus grand que
     cod1 selon cet ordre et compatible avec les nbCoups premières lignes de cod et rep si ce code existe,
     sinon met dans cod1 le code ne contenant que des "0" et retourne faux
     */
    public static boolean passeCodeSuivantLexicoCompat(int[] cod1, int[][] cod, int[][] rep, int nbCoups,int nbCouleurs) {
        while (passeCodeSuivantLexico(cod1, nbCouleurs)) {
            if (estCompat(cod1, cod, rep, nbCoups, nbCouleurs)) {
                System.out.println(Arrays.toString(cod1));
                return true;
            }
        }
        return false;
    }

    //___________________________________________________________________

    // manche Ordinateur

    /** pré-requis : numManche >= 2
     action : effectue la (numManche)ème  manche où l'humain est le codeur et l'ordinateur le décodeur
     (le paramètre numManche ne sert que pour l'affichage)
     résultat :
     - 0 si le programme détecte une erreur dans les réponses du joueur humain
     - un nombre supérieur à nbEssaisMax, calculé à partir du dernier essai de l'ordinateur (cf. sujet),
     s'il n'a toujours pas trouvé au bout du nombre maximum d'essais
     - sinon le nombre de codes proposés par l'ordinateur
     */
    public static int mancheOrdinateur(int lgCode, char[] tabCouleurs, int numManche, int nbEssaisMax) {
        System.out.println("Manche numéro : " + numManche);
        int[] cod1 = initTab(lgCode, 0);
        int[] codVide = initTab(lgCode, 0);
        int nbCoups=0;
        int[][] cod = new int[nbEssaisMax][lgCode];
        int[][] rep = new int[nbEssaisMax][2];
        String codMot;
        while (nbCoups < nbEssaisMax) {
            nbCoups++;
            codMot = entiersVersMot(cod1, tabCouleurs);
            System.out.println("Code proposé par l'ordinateur : " + codMot);
            rep[nbCoups-1] = reponseHumain(lgCode);
            cod[nbCoups-1] = copieTab(cod1);
            if (rep[nbCoups-1][0] == lgCode) {
                System.out.println("votre score sur cette manche est de : ");
                return nbCoups;
            }
            if (!passeCodeSuivantLexicoCompat(cod1, cod, rep, nbCoups, tabCouleurs.length)) {
                System.out.println("Tricheur!!! votre score sur cette manche est : 0");
                return 0;
            }
        }
        int score = nbEssaisMax + rep[nbEssaisMax - 1][1]
                + 2 * (lgCode - (rep[nbEssaisMax - 1][0] + rep[nbEssaisMax - 1][1]));
        System.out.print("votre score sur cette manche est de "+score);
        return score;
    }
    //___________________________________________________________________

    //.........................................................................
    // FONCTIONS DE SAISIE POUR LE PROGRAMME PRINCIPAL
    //.........................................................................


    /** pré-requis : aucun
     action : demande au joueur humain de saisir un entier strictement positif,
     avec re-saisie éventuelle jusqu'à ce qu'elle soit correcte
     résultat : l'entier strictement positif saisi
     */
    public static int saisirEntierPositif(){
        int a=Ut.saisirEntier();
        while (a<0){
            Ut.afficher("ERREUR : ENTIER NEGATIF ! Veuillez saisir un entier POSITIF.");
            a=Ut.saisirEntier();
        }
        return a;
    }

    //___________________________________________________________________

    /** pré-requis : aucun
     action : demande au joueur humain de saisir un entier pair strictement positif,
     avec re-saisie éventuelle jusqu'à ce qu'elle soit correcte
     résultat : l'entier pair strictement positif saisi
     */
    public static int saisirEntierPairPositif(){
        Ut.afficherSL("Saisir un entier positif et pair:");
        int a=Ut.saisirEntier();
        while (a%2!=0 || a<0){
            if(a%2!=0 && a<0){
                Ut.afficher("ERREUR : ENTIER NEGATIF et ENTIER IMPAIR ! Veuillez saisir un entier PAIR ET POSITIF : ");
            }
            else if(a%2!=0){
                Ut.afficher("ERREUR : ENTIER IMPAIR ! Veuillez saisir un entier PAIR ET POSITIF : ");
            }
            else if(a<0){
                Ut.afficher("ERREUR : ENTIER NEGATIF ! Veuillez saisir un entier PAIR ET POSITIF : ");
            }
            a=Ut.saisirEntier();
        }
        return a;
    }

    //___________________________________________________________________

    /** pré-requis : aucun
     action : demande au joueur humain de saisir le nombre de couleurs (stricement positif),
     puis les noms de couleurs aux initiales différentes,
     avec re-saisie éventuelle jusqu'à ce qu'elle soit correcte
     résultat : le tableau des initiales des noms de couleurs saisis
     */
    public static char[] saisirCouleurs(){
        Ut.afficherSL("Saisir le nombre de couleurs :");
        int nbCouleur=saisirEntierPositif();
        char[] tabCouleurs= new char[nbCouleur];
        for (int i =0;i<nbCouleur;i++){
            Ut.afficherSL("Saisir la lettre correspondant a la couleur numero "+(i+1));
            char CaractereSaisi=Ut.saisirCaractere();
            while (estPresent(tabCouleurs,CaractereSaisi)) {
                Ut.afficherSL("ERREUR ! Veuillez saisir une couleur differente de ceux precedemment saisis.");
                CaractereSaisi =Ut.saisirCaractere();
            }
            tabCouleurs[i]=CaractereSaisi;
        }
        return tabCouleurs;
    }

    //___________________________________________________________________

    //.........................................................................
    // PROGRAMME PRINCIPAL
    //.........................................................................


    /**CHANGE : ajout de : le nombre d'essais maximum doit être strictement positif
     ******************************************************************************
     action : demande à l'utilisateur de saisir les paramètres de la partie (lgCode, tabCouleurs,
     nbManches, nbEssaisMax),
     effectue la partie et affiche le résultat (identité du gagnant ou match nul).
     La longueur d'un code, le nombre de couleurs et le nombre d'essais maximum doivent être strictement positifs.
     Le nombre de manches doit être un nombre pair strictement positif.
     Les initiales des noms de couleurs doivent être différentes.
     Toute donnée incorrecte doit être re-saisie jusqu'à ce qu'elle soit correcte.
     */
    public static void main (String[] args){
        Ut.afficherSL("Saisie du nombre de pions du code secret ");
        int lgCode=saisirEntierPositif();
        Ut.afficherSL("Saisie du nombre de manches de la partie ");
        int nbManches=saisirEntierPairPositif();
        Ut.afficherSL("Saisie du nombre d essai maximum de codes a proposer ");
        int nbEssaisMax=saisirEntierPositif();
        char[] tabCouleurs=saisirCouleurs();
        int numManches=1;
        int scoreH=0;
        int scoreO=0;
        for (int i=0;i<nbManches;i++) {
            if (i%2==0) {
                scoreH = scoreH+mancheHumain(lgCode, tabCouleurs, numManches, nbEssaisMax);
            }else {
                scoreO = scoreO+mancheOrdinateur(lgCode, tabCouleurs, numManches, nbEssaisMax);
            }
            numManches++;
        }
        Ut.afficherSL("Score de l'ordinateur : "+scoreH);
        Ut.afficherSL("Score de l'humain: "+scoreO);




    } // fin main

    //___________________________________________________________________

} // fin MasterMindBase